syntax on
set tabstop=2
set expandtab
set shiftwidth=2
set shiftround
set nocompatible
set ruler

filetype plugin on

syntax enable
colorscheme monokai
set number

au BufRead,BufNewFile *.py set filetype=python
au BufRead,BufNewFile *.pde set filetype=arduino
au BufRead,BufNewFile *.ino set filetype=arduino
au BufRead,BufNewFile *.hcl set filetype=hcl
au BufRead,BufNewFile *.tf set filetype=hcl
au BufRead,BufNewFile *.nomad set filetype=hcl
au BufRead,BufNewFile *.yml set filetype=yaml
au BufRead,BufNewFile *.yaml set filetype=yaml

autocmd FileType ruby setlocal ts=2 sts=2 sw=2
autocmd FileType python setlocal ts=4 sts=4 sw=4
autocmd FileType arduino colorscheme arduino
autocmd FileType hcl colorscheme hcl
autocmd FileType yaml setlocal ts=2 sts=2 sw=2

"highlight OverLength ctermbg=gray ctermfg=white guibg=#592929
"match OverLength /\%81v.\+/
let &colorcolumn=join(range(81,999),",")
let &colorcolumn="80,".join(range(400,999),",")

