#!/usr/bin/env bash

export EDITOR=vim

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Syntax highlighting in less:
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
export LESS=' -R '

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

function _update_ps1() {
    PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# This specifies an mfa alias which calls oathtool and expects one argument:
# name of a file inside your ~/.aws/ directory which contains a string that
# is the base for computing your time-based one-time passwords.
# To continue the AWS-based example, you can find the code in the AWS console
# while setting up a new virtual MFA device.
function mfa () {
  oathtool --base32 --totp "$(cat ~/.aws/$1.mfa)" ;
}

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias lt='ls -lrt'
alias lZ='ls -lZ'
alias rsync='rsync -var --progress'

# Git:
alias ga='git add'
alias gap='git add -p'
alias gbr='git branch'
alias gca='git commit --amend'
alias gct='git checkout'
alias gps='git push'
alias gpu='git push -u origin'
alias gadd='git add'
alias gcom='git commit -m'
alias gdiff='git diff'
alias glog='git log'
alias gstatus='git status'

# Cloud Profile Manager:
alias cl='cloud list'
alias cu='cloud use'

# Mercurial :
alias hgcom='hg commit -m'

# Chef:
alias kitchen='chef exec kitchen'
alias inspec='chef exec inspec'

# SSH:
alias ssh_hosts="grep 'Host ' ~/.ssh/config|awk '{print $2}\'|sort"

# Cypress:
alias cypress="$(npm bin)/cypress open"


# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    # shellcheck source=/etc/bash_completion
    . /etc/bash_completion
fi

# Golang :
if [ -d "$HOME/.go" ]; then
  export GOPATH=$HOME/.go/src:$HOME/repos/github/hcl-lint
  export GOBIN=$HOME/.go/bin
  export PATH=$PATH:$GOBIN
fi

# Terraform :
if [ -f "/usr/local/bin/terraform" ]; then
  alias tflint='terraform fmt -check=true -write=false -diff=true'
fi

# Vault :
if [ -d "/usr/local/bin/vault" ]; then
  export VAULT_ADDR='http://127.0.0.1:8200'
fi

if [ -f "${PWD}/.envrc" ]; then
  # shellcheck source=${PWD}/.envrc
  source "${PWD}/.envrc"
fi

# Postman:
if [ -f "/usr/local/bin/Postman" ]; then
  export PATH=$PATH:/usr/local/bin/Postman
fi

xinput|grep "Expert Wireless TB Mouse" > /dev/null
if [ $? -eq 0 ]; then
        device=$(xinput|grep TB|head -1|awk '{print $7}'|cut -d '=' -f 2)
        xinput set-button-map $device 1 8 3 4 5 6 7 2 9
fi

# Virtualenvwrapper:
if [ -d "/usr/local/bin/virtualenvwrapper.sh" ]; then
  export WORKON_HOME=~/.virtualenvs
  source /usr/local/bin/virtualenvwrapper.sh
fi


shortcuts() {
  echo "0: git status"
  echo "1: git commit"
  echo "2: git pull"
  echo "3: git checkout"
  echo "4: git commit --amend"
  echo "5: git pull"
  echo "6: (play/pause)"
  echo "7: (Volume On/Off)"
  echo "8: (Volume -)"
  echo "9: (Volume +)"
}

if [ -f "$HOME/.pyenv/bin/pyenv" ]; then
  export PATH="$HOME/.pyenv/bin:$PATH"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
fi

if [ -f "$HOME/.aon3drc" ]; then
  source $HOME/.aon3drc
fi

